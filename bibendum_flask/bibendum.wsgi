#! /usr/bin/python3
import logging
import sys
import os
import pathlib
logging.basicConfig(stream=sys.stderr, level=logging.DEBUG, encoding="UTF-8")
logging.info("Starting !")

path = pathlib.Path(os.path.realpath(__file__))
sys.path.append(str(path.parent.parent))
logging.debug(f"Path {sys.path}") 

from bibendum_flask.bibendum import app as application
application.secret_key = "Ca,,q]dey9`'}26OxW6@DCsKn"
