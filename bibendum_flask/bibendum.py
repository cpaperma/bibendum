from flask import Flask
import re
import json
import bibendum
from bibendum.crossref_driver import Fetch as CrossrefFetch
from bibendum.datacite_driver import Fetch as DataciteFetch
from bibendum.dx_doi_driver import Fetch as DxDoiFetch
from bibendum.sql import sql
import concurrent.futures
import bibendum.bibtex_utils as bibtex_utils
import logging

short_label_extract = re.compile("([a-z]+)([0-9]+)([a-z]+)([0-9]*)")
db = sql()
app = Flask(__name__)

fetchs = [CrossrefFetch, DataciteFetch]

@app.route("/doi/<shortlabel>")
def doi_shortlabel(shortlabel):
    doi = db.get_shortlabel(shortlabel)
    if not doi:
        try:
            doi = fetch_doi(shortlabel)
        except ValueError as E:
            return E.args[0], 404
        db.save_shortlabel(shortlabel, doi)
    return doi


def fetch_doi(shortlabel):
    shortlabel = shortlabel.lower()
    m = short_label_extract.match(shortlabel)
    if not m:
        raise ValueError("badly formated shortlabel")
    author, year, title, nb = m.groups()
    if not all((author, year, title)):
        raise ValueError(f"badly formated shortlabel\n{author},{year},{title}")
    if not nb:
        nb = 0
    else:
        nb = int(nb)
    with concurrent.futures.ThreadPoolExecutor(max_workers=len(fetchs)) as executor:
        dois = set(sum(executor.map(lambda e:e(author, title, year, db), fetchs), start=[]))
    dois = sorted(dois)
    if len(dois) <= nb:
        raise ValueError(f"No reference found")
    return dois[nb]

@app.route("/dois/<shortlabel>")
def fetchall_dois(shortlabel):
    shortlabel = shortlabel.lower()
    m = short_label_extract.match(shortlabel)
    if not m:
        return "badly formated shortlabel", 404
    author, year, title, nb = m.groups()
    if not all((author, year, title)):
        return f"badly formated shortlabel\n{author},{year},{title}", 404
    with concurrent.futures.ThreadPoolExecutor(max_workers=len(fetchs)) as executor:
        dois = set(sum(executor.map(lambda e:e(author, title, year), fetchs), start=[]))
    return {"dois": sorted(dois)}

@app.route("/bibtex/<shortlabel>")
def bibtex(shortlabel):
    bib = db.get_bibtex(shortlabel)
    if not bib:
        doi = doi_shortlabel(shortlabel)
        if type(doi) is tuple:
            return doi
        if doi:
            bib = db.get_bibtex_from_doi(doi)
        if not bib:
            bib = fetch_bibtex(shortlabel)
    return bibtex_utils.switchheader(bib, shortlabel)

def fetch_bibtex(shortlabel):
    try:
        doi = doi_shortlabel(shortlabel)
    except ValueError as E:
        return E.args[0], 404
    logging.debug(doi)
    bibdoc = DxDoiFetch(doi, db)
    return bibdoc

@app.route("/bibtexes/<shortlabel>")
def fetchall_bibtex(shortlabel):
    dois = fetchall_dois(shortlabel)["dois"]
    with concurrent.futures.ThreadPoolExecutor(max_workers=len(dois)) as executor:
        bibs = list(executor.map(DxDoiFetch, dois))
    bibs = list(map(lambda e:bibtex_utils.switchheader(e[1], f"{shortlabel}{e[0]+1}"), enumerate(bibs)))
    return {"bibs": bibs}

@app.route("/ryalive")
def alive():
    return "Yes, master."

if __name__ == "__main__":
    app.run()
    
