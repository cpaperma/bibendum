/* 

 When is added to the DATABASE 
 we store the raw answer as a JSON document, 
 the full HTTP query executed and timestamped
*/

CREATE EXTENSION plpython3u;

CREATE TABLE raw_document(
	id INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY, 
	query TEXT, 
	document JSONB, 
	date DATE DEFAULT now()
);


--- The DOI table.

CREATE TABLE dois (
	id INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY, 
	doi TEXT UNIQUE,
	source_document INTEGER REFERENCES raw_document(id)
);


/* 
	bibtex computed for the associated doi.
*/
CREATE TABLE bibentry(
	doi_id INTEGER REFERENCES dois(id) PRIMARY KEY, 
	bibtex TEXT,
	metadata JSON DEFAULT NULL
);


/*
 We associate documents to the dois we can find in it. 
 Many to Many relationship here.
 We probably only want to fetch documents that have inserted dois.
*/

CREATE TABLE documents_to_dois(
	doi_id INTEGER REFERENCES dois(id),
	document_id INTEGER REFERENCES raw_document(id),
	UNIQUE(doi_id, document_id)
);

/* 
	entity table store standardized limitedmeta data about an entity
*/

CREATE TABLE entity(
	doi_id INTEGER REFERENCES dois(id) PRIMARY KEY,
	authors TEXT[],
	title_words TEXT[],
	year INT
);

CREATE INDEX entity_authors ON entity USING GIN(authors);
CREATE INDEX entity_words ON entity USING GIN(title_words);
CREATE INDEX entity_year ON entity(year);

/*
	Known aliases
*/
CREATE TABLE aliases(
	key TEXT PRIMARY KEY,
	namespace TEXT default '',
	doi_id INTEGER REFERENCES dois(id),
	unique (namespace, key)
);

CREATE INDEX doi_to_aliases ON aliases(doi_id, key);

