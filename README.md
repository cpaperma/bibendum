# bibendum

Small app to fetch bibtex from a short label with the format: `LastName.Year.PublicationTerm` where the `.` denote
the concatenation. For isntance `Codd1970Relational`.

* `LastName` is the last name of one of the authors
* `Year` is the publication year
* `PublicatonTerm` is one meaningful word in the title of the publication.

In case of ambiguity, an extra integer is used. Ambiguous entries are resolved by sorting
dois under lexicographical order. The `api` is idempotent, every decision taken is recorded
and replayed.


## Example

* The shortlabel `pin1994polynomial` can be queried with `curl "https://paperman.name/bibendum/api/doi/pin1994polynomial"` which returns the DOI `10.1007/3-540-58201-0_87`

* The shortlabel `Pin2008Regular` is ambiguous, the first one by lexicographical order is returned. To get all the possible DOIs:
`curl "https://paperman.name/bibendum/api/dois/pin2009languages"` which returns
`{"dois":["10.1007/978-3-642-02930-1_10","10.25596/jalc-2009-149"]}`.

* We can get directly a bibtex entry as well:
`curl "https://paperman.name/bibendum/api/bibtex/pin1994polynomial"` returns
```
@incollection{pin1994polynomial,
	doi = {10.1007/3-540-58201-0_87},
	url = {https://doi.org/10.1007%2F3-540-58201-0_87},
	year = 1994,
	publisher = {Springer Berlin Heidelberg},
	pages = {424--435},
	author = {Jean -Eric Pin},
	title = {Polynomial closure of group languages and open sets of the Hall topology},
	booktitle = {Automata, Languages and Programming}
}
```

* It works for most CS papers. For instance, `shannon1948communication` gives the following:

```
@article{shannon1948communication,
	doi = {10.1002/j.1538-7305.1948.tb00917.x},
	url = {https://doi.org/10.1002%2Fj.1538-7305.1948.tb00917.x},
	year = 1948,
	month = {oct},
	publisher = {Institute of Electrical and Electronics Engineers ({IEEE})},
	volume = {27},
	number = {4},
	pages = {623--656},
	author = {C. E. Shannon},
	title = {A Mathematical Theory of Communication},
	journal = {Bell System Technical Journal}
}
```

* It works also for papers in other areas, as long as the document is in one of the supported [sources](#Sources). For instance, `Nazzy2019Vowels` gives the following:

```
@article{Nazzi2019Vowels,
	doi = {10.1146/annurev-linguistics-011718-011919},
	url = {https://doi.org/10.1146%2Fannurev-linguistics-011718-011919},
	year = 2019,
	month = {jan},
	publisher = {Annual Reviews},
	volume = {5},
	number = {1},
	pages = {25--47},
	author = {Thierry Nazzi and Anne Cutler},
	title = {How Consonants and Vowels Shape Spoken-Language Recognition},
	journal = {Annual Review of Linguistics}
}
```


## Installation

The program is a [Flask](https://flask.palletsprojects.com/en/2.0.x/) application.

Assuming a Python distribution with `pip`, simply do

```
python3 -m pip install -r requirements.txt
```

For your own web server, look at how to add a `wsgi` script.
In Apache (my solution), I simply add the following line:

```
WSGIScriptAlias /bibendum/api /path/to/bibendum/flask/bibendum.wsgi
```

## Sources {#Sources}

This program uses:

- the [Crossref](https://crossref.org) API
- the [Datacite](https://datacite.org/) API
- the service [dx.doi.org](https://dx.doi.org/) to generate bibtex


# Thanks

- [Tito](http://nguyentito.eu/)
- [a3nm](https://a3nm.net/)
