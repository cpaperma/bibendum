import pathlib
import os
import logging
import json

dbuser = "bibuser"
dbname = "bibendum"
dbpassword = "password" # TO CHANGE
dbhost = "localhost"
dbport = "5432"
mail = "charles.paperman@univ-lille.fr" # Change if you want to indicate another mail in the user-agent.

p = pathlib.Path(os.path.expanduser("~/.bibendum.json"))
if not p.exists():
    logging.warning(f"No bibendum config file ({p}, using default parameters")
else:
    with open(p) as f:
        d = json.load(f)
    logging.info(f"Config found: {d}")
    globals().update(**d)

